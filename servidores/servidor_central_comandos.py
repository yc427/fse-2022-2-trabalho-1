import json, socket, csv, datetime

# Reads and stores the JSON configuration from each JSON file specified by the user.
json_file = "configuracao_sala_00.json"
configs = []
print("\t--> Digite o nome de cada arquivo JSON de configuração das salas das placas dos servidores distribuidos.")
print("\t--> Apenas tecle Enter para terminar ( Ex: ../configuracoes/configuracao_sala_01.json ).")
while json_file != "":
    json_file = input("\t--> Nome do arquivo: ")
    if(json_file != ""):
        with open(json_file) as configs_file:
            config = json.load(configs_file)
            configs.append(config)

# Will generate a log in 'logs.csv' for each command executed
with open("logs.csv", 'a', newline="") as logs:
    fieldnames      = ["DATA-HORA", "SALA", "EVENTO", "TIPO"]
    writer          = csv.DictWriter(logs, fieldnames)

    # Create the CLI interface
    while True:
        for i in range (1, 51):
            print()

        # Lists all distributed servers added to the user to choose
        print("\n\t--> DIGITE O NUMERO DA SALA\n")
        for i in range(0, len(configs)):
            config_i  = configs[i]
            name    = config_i["nome"]
            ip      = config_i["ip_servidor_distribuido"]
            print(f"\t[{i+1}] = ( {ip} ) {name}")

        addr_num = int(input("\n\tNUMERO: "))
        command = 8

        # Show the command options
        while command != 0:
            print("\n\t--> DIGITE O NUMERO DO DISPOSITIVO PARA LIGAR/DESLIGAR\n")
            print("\t[1] = Sistema de Alarme\t\t[2] = Alarme de Incendio")
            print("\t[3] = Lampada 01\t\t[4] = Lampada 02")
            print("\t[5] = Projetor Multimidia\t[6] = Ar condicionado 1 Andar")
            print("\t[7] = Sirene do Alarme (Observar o status)")
            print("\t[0] = TERMINAR")
            command = int(input("\n\tNUMERO: "))
            if(command != 0):
                addr = configs[(addr_num - 1)]
                sck_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sck_send.connect((addr["ip_servidor_distribuido"], addr["porta_servidor_distribuido"]))
                # Send the comand to the distributed server chosen
                current_time = datetime.datetime.now()
                if(command == 1):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Sistema de Alarme"})
                    sck_send.send(bytes("Sistema de Alarme", "UTF-8"))
                    sck_send.close()
                elif(command == 2):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Alarme de Incêndio"})
                    sck_send.send(bytes("Alarme de Incêndio", "UTF-8"))
                    sck_send.close()
                elif(command == 3):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Lâmpada 01"})
                    sck_send.send(bytes("Lâmpada 01", "UTF-8"))
                    sck_send.close()
                elif(command == 4):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Lâmpada 02"})
                    sck_send.send(bytes("Lâmpada 02", "UTF-8"))
                    sck_send.close()
                elif(command == 5):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Projetor Multimidia"})
                    sck_send.send(bytes("Projetor Multimidia", "UTF-8"))
                    sck_send.close()
                elif(command == 6):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Ar-Condicionado (1º Andar)"})
                    sck_send.send(bytes("Ar-Condicionado (1º Andar)", "UTF-8"))
                    sck_send.close()
                elif(command == 7):
                    writer.writerow({"DATA-HORA": current_time, "SALA": addr["nome"], "EVENTO": "COMANDO", "TIPO": "Sirene do Alarme"})
                    sck_send.send(bytes("Sirene do Alarme" , "UTF-8"))
                    sck_send.close()

        for i in range (1, 51):
            print()
