import json, time, socket, threading, csv, datetime
import RPi.GPIO as GPIO, adafruit_dht

# Reads and stores the JSON configuration from the JSON file specified by the user.
configs = ""
print("\t--> Digite o nome do arquivo JSON de configuração da sala da placa")
print("\t--> ( Ex: ../configuracoes/configuracao_sala_01.json )")
json_file = input("\t--> Nome do arquivo: ")
with open(json_file) as configs_file:
    configs = json.load(configs_file)

# Defines the GPIOs from the settings in JSON.
GPIO.setmode(GPIO.BCM)
for outp in configs["outputs"]:
    GPIO.setup(outp["gpio"], GPIO.OUT)
for inp in configs["inputs"]:
    GPIO.setup(inp["gpio"], GPIO.IN)
for wire in configs["sensor_temperatura"]:
    dht_device = adafruit_dht.DHT22(wire["gpio"])

# Will store the GPIOs values.
values = {
    "Sistema de Alarme"             : 0,
    "Alarme de Incêndio"            : 0,
    "Lâmpada 01"                    : 0,
    "Lâmpada 02"                    : 0,
    "Projetor Multimidia"           : 0,
    "Ar-Condicionado (1º Andar)"    : 0,
    "Sirene do Alarme"              : 0,
    "Sensor de Presença"            : 0,
    "Sensor de Fumaça"              : 0,
    "Sensor de Janela"              : 0,
    "Sensor de Porta"               : 0,
    "Quantidade de Pessoas"         : 0,
    "Temperatura"                   : 1.0,
    "Umidade"                       : 1.0
}

# Socket to send the 'values' to the Central Server.
# sck_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# sck_send.connect((configs["ip_servidor_central"], configs["porta_servidor_central"]))

# Socket to receive the 'values' from the Central Server.
sck_recv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sck_recv.bind((configs["ip_servidor_distribuido"], configs["porta_servidor_distribuido"]))
sck_recv.listen()

# Function to receive 'values' from the Central Server in a separate thread.
def recv():
    global values
    # Will generate a log on 'logs.csv' for each time an alarm has been triggered
    with open("logs.csv", 'a', newline="") as logs:
        fieldnames  = ["DATA-HORA", "SALA", "EVENTO", "TIPO"]
        writer      = csv.DictWriter(logs, fieldnames)

        while True:
            conn, addr = sck_recv.accept()
            command = conn.recv(1024).decode("UTF-8")
            if(command == "Sistema de Alarme" and values["Sensor de Presença"] == 1 and values["Sistema de Alarme"] == 0):
                continue
            elif(command == "Sistema de Alarme" and values["Sensor de Janela"] == 1 and values["Sistema de Alarme"] == 0):
                continue
            elif(command == "Sistema de Alarme" and values["Sensor de Porta"] == 1 and values["Sistema de Alarme"] == 0):
                continue
            elif(command == "Alarme de Incêndio" and values["Sensor de Fumaça"] == 1 and values["Alarme de Incêndio"] == 0):
                continue
            elif(values[command] == 1):
                values[command] = 0
            elif(values[command] == 0):
                values[command] = 1

            if(values["Sirene do Alarme"] == 1):
                current_time = datetime.datetime.now()
                if(values["Sistema de Alarme"] == 1 and values["Alarme de Incêndio"] == 1):
                    writer.writerow({"DATA-HORA": current_time, "SALA": configs["nome"], "EVENTO": "ACIONAMENTO DE ALARME", "TIPO": "Sistema de Alarme + Alarme de Incêndio"})
                elif(values["Sistema de Alarme"] == 1):
                    writer.writerow({"DATA-HORA": current_time, "SALA": configs["nome"], "EVENTO": "ACIONAMENTO DE ALARME", "TIPO": "Sistema de Alarme"})
                elif(values["Alarme de Incêndio"] == 1):
                    writer.writerow({"DATA-HORA": current_time, "SALA": configs["nome"], "EVENTO": "ACIONAMENTO DE ALARME", "TIPO": "Alarme de Incêndio"})
                else:
                    writer.writerow({"DATA-HORA": current_time, "SALA": configs["nome"], "EVENTO": "ACIONAMENTO DE ALARME", "TIPO": "Manual"})

# Function to read the 'dht_device'
def read_dht_device():
    global values

    while True:
        values["Temperatura"]   = dht_device.temperature
        values["Umidade"]       = dht_device.humidity
        time.sleep(2)

# Function to signals to turn on the lamps for 15 seconds then turn off them.
turn_on_lamps_15_running = 0
def turn_on_lamps_15():
    global values
    global turn_on_lamps_15_running

    values["Lâmpada 01"] = 1
    values["Lâmpada 02"] = 1
    time.sleep(15)
    values["Lâmpada 01"] = 0
    values["Lâmpada 02"] = 0
    turn_on_lamps_15_running = 0

# Start the thread to receive 'values' from the Central Server
threading._start_new_thread(recv, ())

# Start the thread to read the 'dht_device'
threading._start_new_thread(read_dht_device, ())

# Distributed Server execution loop
while True:
    # Update the GPIOs 'values'.
    for inp in configs["inputs"]:
        # JSON keys
        inp_type    = inp["type"]
        inp_tag     = inp["tag"]
        inp_gpio    = GPIO.input(inp["gpio"])
        val_amt_pes = values["Quantidade de Pessoas"]

        # Change the INPUT 'values'.
        if(inp_type == "contagem"):
            if(inp_tag == "Sensor de Contagem de Pessoas Entrada" and inp_gpio == 1):
                values["Quantidade de Pessoas"] += 1
            elif(inp_tag == "Sensor de Contagem de Pessoas Saída" and inp_gpio == 1 and val_amt_pes > 0):
                values["Quantidade de Pessoas"] -= 1
        else:    
            values[inp_tag] = inp_gpio

        # Change the OUTPUT 'values'.
        if(values["Sensor de Presença"] == 1):
            if(values["Sistema de Alarme"] == 1):
                values["Sirene do Alarme"] = 1
            elif(turn_on_lamps_15_running == 0):
                turn_on_lamps_15_running = 1
                threading._start_new_thread(turn_on_lamps_15, ())

        if(values["Sistema de Alarme"] == 1 and values["Sensor de Janela"] == 1):
            values["Sirene do Alarme"] = 1
        elif(values["Sistema de Alarme"] == 1 and values["Sensor de Porta"] == 1):
            values["Sirene do Alarme"] = 1

        if(values["Alarme de Incêndio"] == 1 and values["Sensor de Fumaça"] == 1):
            values["Sirene do Alarme"] = 1
            

    # Send the 'values' to the GPIOs.
    for outp in configs["outputs"]:
        value = GPIO.LOW
        if(values[outp["tag"]] == 1):
            value = GPIO.HIGH
        GPIO.output(outp["gpio"], value)

    # Sleep because "Sensor de Contagem de Pessoas ..." has a delay
    time.sleep(0.3)

    # Socket to send the 'values' to the Central Server.
    sck_send = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sck_send.connect((configs["ip_servidor_central"], configs["porta_servidor_central"]))
    sck_send.send(bytes(json.dumps(values), "UTF-8"))
    sck_send.close()
