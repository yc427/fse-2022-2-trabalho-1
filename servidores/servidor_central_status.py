import json, socket

# Reads and stores the JSON settings of all JSON files specified by the user.
configs = ""
print("\t--> Digite o nome do arquivo JSON de configuração da sala da placa")
print("\t--> ( Ex: ../configuracoes/configuracao_sala_01.json )")
json_file = input("\t--> Nome do arquivo: ")
with open(json_file) as configs_file:
    configs = json.load(configs_file)

# Will store the values from each Distributed Server
srv_dist_values = {}

# Socket to receive the 'values' from each Distributed Server.
sck_recv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sck_recv.bind((configs["ip_servidor_central"], configs["porta_servidor_central"]))
sck_recv.listen()

# Central Server status main loop
while True:
    # Receive the 'values' from each Distributed Server.
    conn, addr = sck_recv.accept()
    srv_dist_values[addr[0]] = json.loads(conn.recv(1024).decode("UTF-8"))

    # Create the CLI interface
    for i in range (1, 51):
        print()
    num_srv = 1
    for ip in srv_dist_values:
        values_print = srv_dist_values[ip]
        # Systems
        sys1 = values_print["Sistema de Alarme"]
        sys2 = values_print["Alarme de Incêndio"]
        # OUTPUTS
        outp1 = values_print["Lâmpada 01"]
        outp2 = values_print["Lâmpada 02"]
        outp3 = values_print["Projetor Multimidia"]
        outp4 = values_print["Ar-Condicionado (1º Andar)"]
        outp5 = values_print["Sirene do Alarme"]
        # INPUTS
        inp1 = values_print["Sensor de Presença"]
        inp2 = values_print["Sensor de Fumaça"]
        inp3 = values_print["Sensor de Janela"]
        inp4 = values_print["Sensor de Porta"]
        inp5 = values_print["Quantidade de Pessoas"]
        # TEMPERATURE and HUMIDITY
        temp = values_print["Temperatura"]
        humi = values_print["Umidade" ]

        print(f"\n\t--> [{num_srv}] ESTADO DO SERVIDOR DISTRIBUIDO {ip}:\n")
        print(f"\t[{outp5}] = Sirene do Alarme\t\t[{sys1}] = Sistema de Alarme\t\t[{sys2}] = Alarme de Incendio\t[{inp5}] = Quantidade de Pessoas")
        print(f"\t[{inp1}] = Sensor de Presenca\t[{inp2}] = Sensor de Fumaca\t\t[{inp3}] = Sensor de Janela\t\t[{inp4}] = Sensor de Porta")
        print(f"\t[{outp1}] = Lampada 01\t\t[{outp2}] = Lampada 02\t\t[{outp3}] = Projetor Multimidia\t[{outp4}] = Ar condicionado 1 Andar")
        print(f"\t[{temp}] = Temperatura\t\t[{humi}] = Umidade")
        if(inp1 == 1 or inp3 == 1 or inp4):
            print(f"\tOBS: Nao sera possivel ligar o Sistema de Alarme pois ele sera acionado imediatamente")
        if(inp2 == 1):
            print(f"\tOBS: Nao sera possivel ligar o Alarme de Incendio pois ele sera acionado imediatamente")
        num_srv += 1
