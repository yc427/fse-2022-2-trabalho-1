# Trabalho 1

## Referência
"Este trabalho tem por objetivo a criação de um sistema distribuído de automação predial para monitoramento e acionamento de sensores e dispositivos de um prédio com múltiplas salas. O sistema deve ser desenvolvido para funcionar em um conjunto de placas Raspberry Pi com um servidor central responsável pelo controle e interface com o usuário e servidores distribuídos para leitura e acionamento dos dispositivos. Dentre os dispositivos envolvidos estão o monitoramento de temperatura e umidade, sensores de presença, sensores de fumaça, sensores de contagem de pessoas, sensores de abertura e fechamento de portas e janelas, acionamento de lâmpadas, aparelhos de ar-condicionado, alarme e aspersores de água em caso de incêndio." [Trabalho 1 - 2022-2](https://gitlab.com/fse_fga/trabalhos-2022_2/trabalho-1-2022-2)

## Organização

### */servidores*
Pasta contendo os programas para executar o Servidor Central (divido em status e comandos) e as diversas instâncias de um Servidor Distribuído
* */servidor_central_status.py* : O usuário recebe o status de cada Servidor Distríbuido que está conectado a este servidor.
* */servidor_central_comandos.py* : O usuário consegue enviar comandos para um Servidor Distríbuido específico, baseando-se no status de cada um.
* */servidor_distribuido.py* : Cada instância representa uma placa em alguma sala que deverá se comunicar com o Servidor Central
* */logs.csv*: Arquivo CSV contendo todos os logs de todos os comandos executados e as vezes que algum alarme foi acionado

### */configuracoes*
Pasta contendo todos os arquivos que representam as configurações de cada placa (de cada sala) a ser usada para inicializar ou acessar algum servidor.

## Execução
Recomenda-se para facilitar a organização mental abrir duas janelas do **terminal**, uma para executar o Servidor Central (status e comandos) com duas abas e outra para executar todas as N instâncias referentes a algum Servidor Distribuído com N abas.

* Entre na pasta */trabalho-1/servidores* em todas as abas.
    * **cd /trabalho-1/servidores**
* Execute o Servidor Central de status e coloque o arquivo JSON de configuração referente a sala/placa que o servidor será executado.
    * **python3 /servidor_central_status.py**
    * **configuracao_sala_01.json** (exemplo)
* Execute o(s) Servidor(es) Distribuído(s) e coloque o(s) arquivo(s) JSON de configuração referente a(s) sala(as)/placa(as) que o(s) servidor(es) será(ão) executado(s).
    * **python3 /servidor_distribuido.py**
    * **configuracao_sala_01.json** (exemplo)
* O Servidor Central de status começará a receber atualizações do(s) Servidor(es) Distribuído(s) conectado(s) a ele.
* Execute o Servidor Central de comandos e coloque o(s) arquivo(s) JSON de configuração referente a(s) sala(as)/placa(as) que o(s) Servidor(es) Distribuído(s) está(ão) sendo executado(s).
    * **python3 /servidor_central_comandos.py**
    * **configuracao_sala_01.json** (exemplo)
* Escolha um Servidor Distribuído por vez que deseje executar algum comando e depois escolha o dispositivo para ligar/desligar, baseando-se no status referente a este Servidor Distribuído. Repita este passo quantas vezes desejar.
* Para olhar os logs de comandos executados e alarmes acionados, utilize algum editor de texto/planilha par abrir o arquivo *logs.csv* (não necessariamente no terminal).
    * **nano logs.csv** (exemplo em outra aba)